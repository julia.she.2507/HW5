package ru.pochta.steps;

import com.google.inject.Inject;
import io.cucumber.java.ru.Если;
import io.cucumber.java.ru.Пусть;
import io.cucumber.java.ru.Тогда;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import ru.pochta.pages.PochtaMainPage;

public class PochtaMainPageSteps {
    String expected = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";
    @Inject
    private PochtaMainPage pochtaMainPage;
    @Пусть("Открыта страница https://www.pochta.ru/ в браузере")
    public void openMainPage() {
        pochtaMainPage.open();
    }

    @Если("Перейти на страницу авторизации")
    public void LogIn() {
        pochtaMainPage.LogIn();
    }

    @Если("Ввести имя пользователя {string}")
    public void enterUser(String user) {
        pochtaMainPage.enterUserName(user);
    }

    @Если("Ввести пароль {string}")
    public void enterPassword(String password) {
        pochtaMainPage.enterUserPassword(password);
    }

    @Если("Перейти на вкладку Мои отправления")
    public void MyShipments() {
        pochtaMainPage.tabMyShipments();
    }

    @Тогда("Проверить текст")
    public void checkText() {
        Assertions.assertEquals(expected, pochtaMainPage.checkText());
     }



}
