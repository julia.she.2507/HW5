package ru.pochta.pages;

import com.google.inject.Inject;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import support.GuiceExample;

public class PochtaMainPage extends BasePage<PochtaMainPage> {
    @FindBy(css = "a[href='/api/auth/login']")
    private WebElement searchLogIn;
    @FindBy(id = "username")
    private WebElement user;
    @FindBy(id = "userpassword")
    private WebElement password;
    @FindBy(xpath = "//div[@title = '79917133197']")
    private WebElement myShipments;
    @FindBy(css = "#tippy-1 a[href='/tracking']")
    private WebElement tracking;
    @FindBy(xpath = "//*[@id='page-tracking']/div/div[2]/div[2]/div[1]/div/div[1]/div/p")
    private WebElement text;

    @Inject
    public PochtaMainPage(GuiceExample guiceExample) {
        super(guiceExample, "");
    }


    public void LogIn() {
//        guiceExample.driver.findElement(By.cssSelector("a[href='/api/auth/login")).sendKeys(Keys.ENTER);
        searchLogIn.sendKeys(Keys.ENTER);
    }

    public void enterUserName(String username) {
        user.sendKeys(username);
    }


    public void enterUserPassword(String userpassword) {
        password.sendKeys(userpassword + Keys.ENTER);
    }

    public void tabMyShipments() {
        Actions actions = new Actions(guiceExample.driver);
//        try {
//             Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        WebDriverWait wait = new WebDriverWait(guiceExample.driver, 10);
        wait.until(ExpectedConditions.visibilityOf(myShipments));
        actions.moveToElement(myShipments).build().perform();

//        второй вариант работы с ожиданием
//        actions.moveToElement(wait.until(ExpectedConditions.visibilityOf(myShipments))).build().perform();

        wait.until(ExpectedConditions.visibilityOf(tracking));
        tracking.click();
    }

    public String checkText() {
        WebDriverWait wait = new WebDriverWait(guiceExample.driver, 10);
        String s = wait.until(ExpectedConditions.visibilityOf(text)).getText();
//        String s = "Сохраняйте отправления, чтобы узнавать по электронной почте, где они находятся";
        return s;
    }
}